class ReportingController < ApplicationController

  def request_report
    ip = params[:ip]
    port = params[:port]
    verify = Rails.root.join(*%w( vendor verify.sh ))
    render :json => { output: `#{verify} #{ip} #{port}` }
  end

end
